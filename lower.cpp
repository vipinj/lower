#include<iostream>
#include<fstream>
#include<string>
#include<algorithm>

static inline char to_lower(const char temp){
  if(temp>='A' &&  temp <='Z'){
    return (char)((int)temp+32);
  }
  else{
    return temp;
  }
}

int main(int argc, char** argv){
  if(argc!=3){
    std::cout<<"Sample Usage: lower <input_file> <output_file>"<<std::endl;
    return 0;
  }
  std::ifstream in;
  std::ofstream of;
  in.open(argv[1], std::ios::in);
  of.open(argv[2], std::ios::out);
  if(!in.is_open()){
    std::cout<<"Input File Error"<<std::endl;
    return 0;
  }
  if(!of.is_open()){
    std::cout<<"Output File Error"<<std::endl;
    return 0;
  }
  while(!in.eof()){
    std::string old_str;
    std::getline(in,old_str);
    //    in>>old_str;
    if(!in.eof()){
      std::string new_str;
      new_str.resize(old_str.size());
      transform(old_str.begin(),old_str.end(),new_str.begin(),to_lower);
      of<<new_str<<std::endl;
    }
  }
  return 0;
}
  
